using System.IO;
using System.Threading.Tasks;
using Library.Domain.Entities;
using Library.Domain.Options;
using Library.Domain.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GridFS; 

namespace Library.Infrastructure.Repositories
{
    public class FormFileRepository :  IFileRepository
    {
        protected readonly MongoClient _client;
        protected IMongoDatabase _database;
        private readonly GridFSBucket _bucket;
        private readonly ILogger<FormFileRepository> logger;
        public FormFileRepository(IMongoOptions options, ILogger<FormFileRepository> logger)
        {
            _client = new MongoClient(options.ConnectionString);
            _database = _client.GetDatabase(options.DatabaseName);
            _bucket = new GridFSBucket(_database, new GridFSBucketOptions { 
                ChunkSizeBytes = 261120,
            });
            this.logger = logger;
        }

        public async Task<string> AddFile(IFormFile formFile)
        {
            return null;
            //var stream = await StreamHelper.FileToStream(formFile);
            //var upload = await _bucket.UploadFromBytesAsync(formFile.FileName, stream.ToArray(), new GridFSUploadOptions()
            //{
            //    ChunkSizeBytes = 261120,
            //});
            //stream.Close();
            //return upload.ToString();
        }

        public Domain.Entities.File GetFile(string Id)
        {
            // _database.GridFs
            logger.LogInformation("staring to get the file info");
            logger.LogInformation(Id);
            var filter = Builders<GridFSFileInfo>.Filter.Eq("_id",ObjectId.Parse( Id));
            var file = _bucket.Find(filter).FirstOrDefault();
            logger.LogInformation("finish to get the file info");
            logger.LogInformation(file.Id.ToString());
            logger.LogInformation(file.Filename);
            var ffile = new Domain.Entities.File()
            {
                Filename = file.Filename,
                Size = file.Length,
                FilesId = file.Id.ToString()
            };
            
            return ffile;
        }

        public async Task<byte[]> DownloadFileById(string id)
        {
            try
            {
                logger.LogInformation("staring to download the file");
                var objectId = ObjectId.Parse(id);
                logger.LogInformation("parsing the object id");
                logger.LogInformation(objectId.ToString());
                logger.LogInformation("getting from the bucket with task");
                var foo = _bucket.DownloadAsBytesAsync(objectId);
                logger.LogInformation("getting from the bucket with task wait for it");
                Task.WaitAll(foo);
                logger.LogInformation("getting from the bucket with task wait for it...");
                if (foo.IsCompleted)
                    return foo.Result;
                logger.LogInformation("getting from the bucket");
                var b = await _bucket.DownloadAsBytesAsync(objectId,cancellationToken: new System.Threading.CancellationToken());
                //var bytes = _bucket.DownloadAsBytes(BsonValue.Create(id));
                logger.LogInformation("finishing to download the file");
                return b;
            }
            catch (System.Exception)
            {
                return null;
            }
        }

        public async Task<byte[]> DownloadFileByName(string fileName)
        {
            logger.LogInformation("staring to download the file by name");
            logger.LogInformation("getting from the bucket");
            logger.LogInformation(fileName);
            var b = await _bucket.DownloadAsBytesByNameAsync(fileName);
            logger.LogInformation("finishing to download the file");
            //var bytes = _bucket.DownloadAsBytes(BsonValue.Create(id));
            return b;
        }

    }
}