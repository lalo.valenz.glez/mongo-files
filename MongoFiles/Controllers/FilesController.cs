﻿using Library.Domain.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MongoFiles.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FilesController : ControllerBase
    {
        private ILogger<FilesController> _logger;
        private readonly IFileRepository _fileRepository;

        public FilesController(ILogger<FilesController> logger, IFileRepository fileRepository)
        {
            _logger = logger;
            _fileRepository = fileRepository;
        }
        [HttpGet]
        public async Task<IActionResult> Get(string id)
        {
            _logger.LogInformation("Get File");
            _logger.LogInformation(id);
            var file = _fileRepository.GetFile(id);
            if(file != null)
            {

            }
            _logger.LogInformation("Get File by id");
            var bytes = await _fileRepository.DownloadFileById(id);
            _logger.LogInformation("Get downloaded File");
            //MemoryStream stream = new MemoryStream(bytes);
            return File(bytes, "application/pdf", file.Filename);
        }
    }
}
